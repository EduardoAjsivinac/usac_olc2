# Clase 27 de Agosto 2022
Explicación de implementación de módulos y funciones. La implementación de la gramática es bastante sencilla.
```python
# Gramatica realizada para modulos
instrmod : mod id { instrucciones }
```

```python
# Grmática realizada para funciones
instrfn : fn id () { instrucciones }

```


## Declaración de vectores
```rust
// Entrada
x = vec![vec![5,3],vec![5,3]];
```
### InstruccionFuncion.py
```python
def ejecutar(self, entorno):
    # fn id ( ) { instrucciones }
    # Se guarda el nodo "instrucciones" para ejecutarla después
    entorno.agregarVariable(self.hojas[1].nombre,
                            self.hojas[5],
                            DataType.generico,
                            SymbolType.funcion
                            )
```

### InstruccionModulo.py
```python
def ejecutar(self, entorno):
    # mod id  { instrucciones }
    # Se crea un nuevo entorno, para obtener la tabla de símbolos ya que dentro de los módulos tienen otros internos y esto no sería posible de no ser con una tabla de simbolos o un equivalente
    ne = Entorno("entorno modulo")
    self.hojas[3].ejecutar(ne)
    entorno.agregarVariable(self.hojas[1].nombre,
                            None,
                            DataType.generico,
                            SymbolType.modulo,
                            ne.tabla_simbolos
                            )
```

### main.py
```python
def print_hi(text_input):
    raiz = analizar_entrada(text_input)
    print(raiz.obtener_dot())
    entorno = Entorno("Global")
    # Se generan las tablas de símbolos generales
    raiz.ejecutar(entorno)

    # Se busca la función "main" dentro del entorno global
    nodoMain = entorno.obtenerValor("main")
    if nodoMain is not None:
        # Se crea un nuevo entorno, y se le asigna el anterior para acceder a módulos y funciones
        ne = Entorno("FuncionMain")
        ne.asignarAnterior(entorno)

        # Se ejecuta el nodo "instrucciones" (el valor de la funcion "main" guardado en la tabla de símbolos)
        nodoMain['valor'].ejecutar(ne)
    else:
        print("No hay funcion main")
    print("Entorno Global", entorno.tabla_simbolos)
```

### Arbol generado por una declaración:
![Arbol generado en la entrada anterior](./Imagenes/arbol_generado.png)

```rust
mod Prueba{
    mod Prueba2{
        fn imprimir(){
            a = 3+4;
        }
    }
}

fn main(){
    a = 3+4;
    b = vec![vec![vec![1,5],vec![1,2]],vec![1,2]];
    c = b[0];
}

```

```json
Entorno Global {
	'Prueba': {
		'valor': None,
		'tipo': < DataType.generico: 5 > ,
		'tipoSimbolo': < SymbolType.modulo: 2 > ,
		'tablaSimbolo': {
			'Prueba2': {
				'valor': None,
				'tipo': < DataType.generico: 5 > ,
				'tipoSimbolo': < SymbolType.modulo: 2 > ,
				'tablaSimbolo': {
					'imprimir': {
						'valor': < parser.nodos.instruccion.InstruccionGenerico.InstruccionGenerico object at 0x0000026A3A677E50 > ,
						'tipo': < DataType.generico: 5 > ,
						'tipoSimbolo': < SymbolType.funcion: 1 > ,
						'tablaSimbolo': None
					}
				}
			}
		}
	},
	'main': {
		'valor': < parser.nodos.instruccion.InstruccionGenerico.InstruccionGenerico object at 0x0000026A3A677AF0 > ,
		'tipo': < DataType.generico: 5 > ,
		'tipoSimbolo': < SymbolType.funcion: 1 > ,
		'tablaSimbolo': None
	}
}
```
