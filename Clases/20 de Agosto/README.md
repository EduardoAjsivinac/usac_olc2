# Clase 20 de Agosto 2022
Explicación de vectores

```python
# Grmática realizada para declaracion de vectores
instrvec : vec! [ listexp ]

listexp : listexp , exp
		| exp

exp : instrvec
```

```python
# Grmática realizada para accesos
exp : id accvec

accvec : accvec [ exp ]
			| [ exp ]
			| 

```
Los vectores y arreglos practicamente tienen el mismo funcionamiento con un poco de cambio en la sintaxis. La única diferencia es que los vectores pueden crecer o disminuir los elementos. 


## Declaración de vectores
```rust
// Entrada
x = vec![vec![5,3],vec![5,3]];
```
### Arbol generado por una declaración:
![Arbol generado en la entrada anterior](./Imagenes/Declaracion.png)

### NodoVector.py
```python
def ejecutar(self, entorno):
    # vec! [ listaexp ]
    self.hojas[2].ejecutar(entorno) # listaexp
    self.tipo = self.hojas[2].tipo # valor de tipoexp
    self.valor = self.hojas[2].valor
```

### NodoListaExpresion.py
```python
def ejecutar(self, entorno):
    # El árbol que construye es una lista de expresiones
    # expresion expresion expresion....
    self.tipo = None
    arreglo = [] # Acá vamos a almacenar todos los datos de los arreglos

    for hoja in self.hojas: 
        # Recorremos cada uno de los nodos y lo ejecutamos
        hoja.ejecutar(entorno)

        # Agregamos todos los valores de las expresiones obtenidas.
        arreglo.append(hoja.valor)

        # Validamos que todas las expresiones sean del mismo tipo
        if self.tipo is None:
            self.tipo = hoja.tipo
        else:
            if self.tipo != hoja.tipo:
                self.tipo = DataType.error

    # Le asignamos el valor como un json (o diccionario)
    if self.tipo != DataType.error:
        self.valor = {
            'tipo': DataType.vector,
            'tipo_elementos': self.tipo,
            'valor': arreglo,
            'tam': len(arreglo)
        }
        self.tipo = DataType.vector
```

## Acceso Vector

```rust
// Entrada
y = x[0][0];
```
### Arbol generado por un acceso:
![Arbol generado en la entrada anterior](./Imagenes/Accesos.png)

```python
def ejecutar(self, entorno):
    # identificador listexp

    # Se ejecuta el identificador y se valida que sea un vector
    self.hojas[0].ejecutar(entorno)
    if self.hojas[0].valor is not None:
        if self.hojas[0].tipo == DataType.vector:

            # Se ejecuta la lista de expresiones (ya se validan que sean del mismo tipo)
            self.hojas[1].ejecutar(entorno)

            # Valida que el tipo de elementos sea un entero, ya que se debe acceder por medio de índices 
            if self.hojas[1].valor['tipo_elementos'] == DataType.int64:

                # Obtenemos el primer vector
                vector = self.hojas[0].valor

                # En este for se validan las N dimensiones posibles. Solo recordar que esto es 
                for pos in self.hojas[1].valor['valor']:

                    # Valida que la variable vector sea de tipo vector
                    if vector['tipo'] == DataType.vector:
                        # Valida índices fuera de rango
                        if pos < len(vector):
                            # Asigna el valor del vector en x posición.
                            vector = vector['valor'][pos]
                        else:
                            print("Fuera de rango")
                    else:
                        print("Error")
                self.valor = vector
            else:
                print("Error en los accesos a posición")
        else:
            print("La variable", self.hojas[0].nombre, "no es un vector")
    else:
        print("La variable", self.hojas[0].nombre, "no existe")
```