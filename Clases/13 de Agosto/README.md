# Clase 13 de Agosto 2022

Explicación de cómo trabajé los entornos:
![Arbol generado por la entrada anterior](./Imagenes/Entornos.png)

Se realizaron ifs y uso de variables con entornos y sub entornos.


### Entorno.py
```python
# Asigna entorno anterior (superior)
def asignarAnterior(self, anterior):
    self.entorno_anterior = anterior

#Busca variable en tablas de simbolos
def obtenerValor(self, nombre):
    # Busca la variable en el entorno actual
    simbolo = self.tabla_simbolos.get(nombre, None)
    if simbolo != None:
        # Si la encontro, la retorna
        return simbolo
    # Busca en entornos anteriores (superiores)
    tmp_anterior = self.entorno_anterior
    while tmp_anterior != None:
        simbolo = tmp_anterior.tabla_simbolos.get(nombre, None)
        if simbolo != None:
            return simbolo
    return None
```

### InstruccionIdentificador.py
```python
def ejecutar(self, entorno):
    # if expresion { instrucciones }
    # if bool { instrucciones }
    self.hojas[1].ejecutar(entorno)
    if self.hojas[1].tipo == DataType.boolean:
        # Se debe validar que el tipo sea booleano y el valor verdadero para ejecutar las instrucciones
        if self.hojas[1].valor:
            ne = Entorno("entornoif") # Se crea un nuevo entorno
            ne.asignarAnterior(entorno)
            self.hojas[3].ejecutar(ne) # Ejecuta instrucciones con nuevo entorno
            print("Entorno de if", ne.tabla_simbolos)
        return
    print('El tipo de dato debe ser booleano ')
```

### TerminalIdentificador.py
```python
def ejecutar(self, entorno):
    # Buscamos la variable en el entorno
    valvar = entorno.obtenerValor(self.nombre)
    if valvar != None:
        # Le asignamos valor y tipo de dato al nodo actual.
        self.valor = valvar['valor']
        self.tipo = valvar['tipo']
```

### Entrada.rs
```rust
a = 10+1;
c = "hola {} mundo";
e = 10 + a;
if 5>4 {
    d=3+9+a;
}
```

### Arbol Generado
![Arbol generado por la entrada anterior](./Imagenes/Clase_13-08-2022.png)

### Tabla de símbolos de cada entorno:
```bach
Entorno de if : {
	'd': {
		'valor': 23,
		'tipo': < DataType.int64: 1 >
	}
}

Entorno Global :  {
	'a': {
		'valor': 11,
		'tipo': < DataType.int64: 1 >
	},
	'c': {
		'valor': 'hola {} mundo',
		'tipo': 'CADENA'
	},
	'e': {
		'valor': 21,
		'tipo': < DataType.int64: 1 >
	}
}
```