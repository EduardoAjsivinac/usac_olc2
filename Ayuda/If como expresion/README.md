# If como expresión
Se implementó el if como expresión (faltan validaciones mínimas, como else obligatorio)

### Gramática
```python
instruccion : instrif

# La produccion instruccionesexp es diferente a la lista de epxresiones.
instrif : IF expresion LLAVEA instruccionesexp LLAVEC instrelse

# La producción instrelse se vuelve recursiva
instrelse : ELSE LLAVEA instruccionesexp LLAVEC
        | instrelseif 

# Se reconoce else if o ninguna instruccion y regresa a la instrucción else
instrelseif : ELSE IF expresion LLAVEA instruccionesexp LLAVEC instrelse
        | 

# Y la instrucción IF se asigna a expresión
expresion : instrif

# Creamos la lista de expresiones (se volverá recursiva)
instruccionesexp :  instruccion instruccionesexpfin
                    | instruccionesexpfin 

# Obligamos a que una expresión (en caso de que venga) esté al final del if
instruccionesexpfin :  instruccionesexp
                    | expresion
                    |
```

## Nuevas clases y archivos modificados
### NodoExpresion.py
El nodo expresión tiene un único hijo y el objetivo es obtener un tipo de nodo para el retorno.
```python
def ejecutar(self, entorno):
    self.hojas[0].ejecutar(entorno)
    self.copiar_valorhoja(0)
    self.es_expresion = True
```

### Nodo.py
Se agregó un método que copiará el valor y tipo de un hijo.
```python
def copiar_valorhoja(self, nohoja):
    if nohoja < len(self.hojas):
        self.valor = self.hojas[nohoja].valor
        self.tipo = self.hojas[nohoja].tipo
```

### InstruccionIf.py
Se agregó la asignación del último valor y las instrucciones else/else if
```python
def ejecutar(self, entorno):
    self.hojas[1].ejecutar(entorno)
    if self.hojas[1].tipo == DataType.boolean:
        if self.hojas[1].valor:
            ne = Entorno("entorno if")
            ne.asignarAnterior(entorno)
            self.hojas[3].ejecutar(ne)

            # Se copian los valores de la última hoja, por lo tanto tenemos el valor de una expresión, si es que viene
            self.copiar_valorhoja(3) 
            print("Entorno de if", ne.tabla_simbolos)
            return
        else: # Se ejecuta solo si el valor de expresión es falsa

            # Se agrega el else o else if. Siempre será el 6to nodo según la construcción de la gramática
            # if expresion  { instrucciones } nodoelseif
            if len(self.hojas) == 6:
                self.hojas[5].ejecutar(entorno)
                self.copiar_valorhoja(5)
            return
    print('El tipo de dato debe ser booleano ')
```

### InstruccionElseIf.py
Es prácticamente lo mismo que la instrucción If pero con cambio de posición de los hijos (expresiones e instrucciones). Se podría usar la misma clase corriendo un nodo (ya que ahora es else if, y no solo if).
```python
def ejecutar(self, entorno):
    # else if expresion { instrucciones }
    # else if expresion { instrucciones } else/elseif
    self.hojas[2].ejecutar(entorno)
    if self.hojas[2].tipo == DataType.boolean:
        if self.hojas[2].valor:
            ne = Entorno("entornoif")
            ne.asignarAnterior(entorno)
            self.hojas[4].ejecutar(ne)
            self.copiar_valorhoja(4)
            print("Entorno de elseif", ne.tabla_simbolos)
            return
        else:
            if len(self.hojas) == 7:
                self.hojas[6].ejecutar(entorno)
                self.copiar_valorhoja(6)
            return
    print('El tipo de dato debe ser booleano ')
```

### InstruccionElse.py
Únicamente se ejecutan las instrucciones, ya que si llega a este punto, ya ejecutó las validaciones anteriores.
```python
def ejecutar(self, entorno):
    self.hojas[2].ejecutar(entorno)
    self.copiar_valorhoja(2)
```

### Entorno.py
Se agregaron las validaciones para agregar una nueva variable. Se busca que exista (ambiente local y anteriores) para asignarle valor o sino, la agrega.
```python
def agregarVariable(self, nombre_variable, valor, tipo):
    # Validaciones
    simbolo = {'valor': valor, 'tipo': tipo}
    if self.tabla_simbolos.get(nombre_variable, None) != None:
        self.tabla_simbolos[nombre_variable] = simbolo
        return
    tmp_anterior = self.entorno_anterior
    while tmp_anterior is not None:
        if tmp_anterior.tabla_simbolos.get(nombre_variable, None) is not None:
            tmp_anterior.tabla_simbolos[nombre_variable] = simbolo
            return
        tmp_anterior = tmp_anterior.entorno_anterior
    self.tabla_simbolos[nombre_variable] = simbolo
```


### Entrada.rs
```rust
a = 10+1;
c = "hola {} mundo";
d = 10 + a;
if 1>4 {
    d=3+9+a;
    w=3+9+a;
    if 5>4 {
       d=3+9+a;
    }
    5
} else if 8>7 {
    d=10+9+a;
}else if 9>7 {
    d=10+10+a;
}else{
    d=10+10+a;
}
s = if 1>4 {
    d=3+9+a;
    w=3+9+a;
    5
}else{
    w=3+9+a;
    7
};
```

### Arbol Generado
![Arbol generado por la entrada anterior](./Imagenes/ArbolGenerado.svg)

### Tabla de símbolos de cada entorno:
```bach
Entorno Global {
	'a': {
		'valor': 11,
		'tipo': < DataType.int64: 1 >
	},
	'c': {
		'valor': 'hola {} mundo',
		'tipo': 'CADENA'
	},
	'd': {
		'valor': 30,
		'tipo': < DataType.int64: 1 >
	},
	'w': {
		'valor': 23,
		'tipo': < DataType.int64: 1 >
	},
	's': {
		'valor': 7,
		'tipo': < DataType.int64: 1 >
	}
}
```