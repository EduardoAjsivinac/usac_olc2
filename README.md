# Compiladores 2 Seccion D

* ### Clases:
    Dentro de las carpeta de Clase encontraras una pequeña explicación de lo realizado.
    * [Sabado 13 de Agosto del 2022](./Clases/13%20de%20Agosto/)
    * [Sabado 20 de Agosto del 2022](./Clases/20%20de%20Agosto/)
    * [Sabado 27 de Agosto del 2022](./Clases/27%20de%20Agosto/)
* ### Ayudas:
    Dentro de las carpeta de Clase encontraras una pequeña explicación de lo realizado.
    * [Implementación de Ifs como expresión](./Ayuda/If%20como%20expresion/)
* ### Codigo Fuente
    Dentro de esta carpeta se encuentra el código fuente.
    * [Codigo Fuente](./RUSTParser/)
* ### Entradas
    Acá podrás encontrar archivos para probar los analizadores. Se espera que se actualice constantemente. El archivo de "entrada.rs" es el que se está utilizando actualmente en laboratorio. NO CONTIENE LA ESTRUCTURA DEL PROYECTO o PUEDE CONTENER SINTAXIS NO COHERENTE CON EL ENUNCIADO.
    * [entrada.rs](./RUSTParser/entradas/entrada.rs)
    * ### Archivos de Entrada (Clase)
    
        En este directorio debemos agregar (estudiantes y auxiliar) los archivos de entrada RELACIONADOS CON LA ESTRUCTURA DEL PROYECTO para que todos podamos probar los archivos y así colaborar unos con otros.
        * [Basico.rs](./RUSTParser/Archivos%20de%20Entrada%20-%20Conjunto/Funciones/FuncionesIntermedias.rs)
        * [Intermedio.rs](./dir3)