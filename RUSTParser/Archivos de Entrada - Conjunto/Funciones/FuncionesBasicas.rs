fn main() {
    //Agregar lo que consideren:
    
    // Operaciones aritmeticas, booleanas, declaraciones y asignaciones
    let cadena1: String = "imprimir".to_string();
    let aritmetica  = 7 - (5 + 10 * (2 + 4 * (5 + 2 * 3)) - 8 * 3 * 3) + 50 * (6 * 2);
    let booleana = true || false && true && (false || true) && false || (true || true);
    println!("{}", cadena1); // Imprime "imprimir"
    println!("{}", aritmetica); // Imprime 214

    // If's
    let a:i64 = 25;
    let b:i64 = 50;
    let c:i64 = 75;

    if booleana {
        println!("La suma de a + b es {}", a+b); // 75
    }


    if a > c || b < c {
        println!("Imprime");
    }else{
        println!("No imprime")
    }

    let cadena1: String = "Cadena uno".to_string();
    let cadena2: &str = "Cadena dos";
    let cadena3: String = cadena2.to_owned();
    println!("{}",cadena1);
    println!("{}",cadena2);
    println!("{}",cadena3);
    println!("----------------");
    println!("Nativas");

    let bflotante : f64 = 8 as f64;
    let cadenacopia: String = cadena1.clone();
    println!("Absoluto 1: {}", (-1.5 as i64).abs());
    println!("Absoluto 2: {}", (a).abs());
    println!("Raiz 1: {}",(4 as f64).sqrt());
    println!("Raiz 2: {}",bflotante.sqrt());
    println!("Cadena Copia: {}",cadenacopia);
}