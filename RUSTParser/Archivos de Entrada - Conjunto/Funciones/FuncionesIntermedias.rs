fn main() {
    let mut varmutable: i64 = 78;

    println!("IF");

    if varmutable > 50 {
        println!("Si sale :3");
    }else if true && (varmutable == 56) {
        println!("Me tengo que esforzar, ya casi :3");
    } else {
        println!("Aún no, pero se puede");
    }

    varmutable = 90;
    if varmutable > 0 {
        println!("Vamos por buen camino");
        if varmutable == 7 {
            println!("Aun no");
        } else if varmutable > 10 && varmutable < 100 {
            println!("Casi casi");
            if varmutable == 90 {
                println!("Excelente ");
                varmutable = 20000;
            }

        } else {
            println!("Sigo intentando");
        }
    }else if varmutable <= 3 {
        println!("Upss, nos pasamos");
        if true && (varmutable == 1) {
            println!("Nop, revisemos los ifs");
        } else if varmutable > 10 {
            println!("Nop, revisemos los ifs");
        } else {
            println!("Solo debemos revisar algunos ifs");
        }
    } else if varmutable != varmutable {
        println!("Upss, nos pasamos");
        if true && (varmutable == -1) {
            println!("Nop, revisemos los ifs");
        } else if varmutable > 10 {
            println!("Nop, revisemos los ifs");
        } else {
            println!("Solo debemos revisar algunos ifs");
        }
    }

    println!("El valor e la variable mutable es : {} y debería ser 20000", varmutable);
    if (varmutable == 20000){
        println!("Excelente, los ifs están correctos");
    }


    let mut contador : i64 = 100;

    while contador != 0 {

        if (contador == 0) {
             println!(" El contador llegó a {}", contador);
        } else if (index > 50) {
            contador = contador / 2 + 20;
        } else {
            contador = contador -1;
        }
        println!("Contador: {}",contador);
    }


}

/*
IF
Si sale :3
Vamos por buen camino
Casi casi
Excelente
El valor e la variable mutable es : 20000 y debería ser 20000
Excelente, los ifs están correctos
Contador: 70
Contador: 55
Contador: 47
Contador: 46
Contador: 45
Contador: 44
Contador: 43
Contador: 42
Contador: 41
Contador: 40
Contador: 39
Contador: 38
Contador: 37
Contador: 36
Contador: 35
Contador: 34
Contador: 33
Contador: 32
Contador: 31
Contador: 30
Contador: 29
Contador: 28
Contador: 27
Contador: 26
Contador: 25
Contador: 24
Contador: 23
Contador: 22
Contador: 21
Contador: 20
Contador: 19
Contador: 18
Contador: 17
Contador: 16
Contador: 15
Contador: 14
Contador: 13
Contador: 12
Contador: 11
Contador: 10
Contador: 9
Contador: 8
Contador: 7
Contador: 6
Contador: 5
Contador: 4
Contador: 3
Contador: 2
Contador: 1
Contador: 0
*/