from .TerminalEntero import TerminalEntero
from .TerminalGenerico import TerminalGenerico
from .TerminalIdentificador import TerminalIdentificador
from .TerminalCadena import TerminalCadena
from .TerminalDecimal import TerminalDecimal