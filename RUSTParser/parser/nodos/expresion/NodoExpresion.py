from parser.nodos.Nodo import Nodo

class NodoExpresion(Nodo):
    def __init__(self, token, id_nodo):
        super().__init__(token, id_nodo)

    def ejecutar(self, entorno):
        self.hojas[0].ejecutar(entorno)
        self.copiar_valorhoja(0)
        self.es_expresion = True

    def crear_tabla_simbolos(self, tabla_simbolos):
        pass

    def crear_codigo3d(self, tabla_simbolos):
        self.hojas[0].crear_codigo3d(tabla_simbolos)
        self.expresion = self.hojas[0].expresion
        self.referencia = self.hojas[0].referencia