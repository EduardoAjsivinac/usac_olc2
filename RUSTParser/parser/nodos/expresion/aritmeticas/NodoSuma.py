from parser.nodos.Nodo import Nodo

class NodoSuma(Nodo):
    def __init__(self, token, id_nodo):
        super().__init__(token, id_nodo)

    def ejecutar(self, entorno):
        # exp + exp
        self.hojas[0].ejecutar(entorno)
        self.hojas[2].ejecutar(entorno)
        self.validar_tipo('+', self.hojas[0].tipo, self.hojas[2].tipo)
        self.valor = self.hojas[0].valor + self.hojas[2].valor

    def crear_tabla_simbolos(self, tabla_simbolos):
        pass

    def crear_codigo3d(self, tabla_simbolos):
        # exp + exp
        self.hojas[0].crear_codigo3d(tabla_simbolos)
        self.hojas[2].crear_codigo3d(tabla_simbolos)
        self.referencia = self.obtener_temporal() # t1, t2, t3, ...
        self.expresion += self.hojas[0].expresion + "\n"
        self.expresion += self.hojas[2].expresion +"\n"
        self.expresion += str(self.referencia) + " = " + str(self.hojas[0].referencia) + " + " + str(self.hojas[2].referencia) +"\n"
        # ref = exp.ref + exp.ref
