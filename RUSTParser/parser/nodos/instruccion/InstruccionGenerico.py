from parser.nodos.Nodo import Nodo

class InstruccionGenerico(Nodo):
    def __init__(self, token, id_nodo):
        super().__init__(token, id_nodo)

    def ejecutar(self, entorno):
        for hoja in self.hojas:
            hoja.ejecutar(entorno)
        self.copiar_valorhoja(len(self.hojas)-1)

    def crear_tabla_simbolos(self, tabla_simbolos):
        for hoja in self.hojas:
            hoja.crear_tabla_simbolos(tabla_simbolos)

    def crear_codigo3d(self, tabla_simbolos):
        for hoja in self.hojas:
            hoja.crear_codigo3d(tabla_simbolos)
            self.expresion += hoja.expresion
