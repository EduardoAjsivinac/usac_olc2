from parser.nodos.Nodo import Nodo
from parser.entorno.Tipos import *
from parser.entorno.TablaSimbolosC3D import *

class InstruccionFuncion(Nodo):
    def __init__(self, token, id_nodo):
        super().__init__(token, id_nodo)

    def ejecutar(self, entorno):
        # FN IDENTIFICADOR ( ) { instrucciones }
        entorno.agregarVariable(self.hojas[1].nombre, # Identificador
                                self.hojas[5], # Guardamos el nodo completo
                                DataType.generico, # Generico, si son funciones tipo "int", se colocarían acá.
                                SymbolType.funcion # Funcion
                                )

    def crear_tabla_simbolos(self, tabla_simbolos):
        tabla_simbolos.agregar_entorno(self.hojas[1].nombre, DataType.generico, SymbolType.funcion)

        # Se agrega el símbolo de retorno al inicio
        tabla_simbolos.agregar_simbolo('return', DataType.retorno, SymbolType.retorno, 1)

        # Se buscan en las instrucciones las asignaciones
        self.hojas[5].crear_tabla_simbolos(tabla_simbolos)

        # Se asigna el tamaño de la función
        tabla_simbolos.asignar_tam_funcion()

        # Se elimina el entorno agregado, solo es para obtener el nombre
        tabla_simbolos.eliminar_entorno()
        tabla_simbolos.reset_stack()

    def crear_codigo3d(self, tabla_simbolos):
        tabla_simbolos.agregar_entornoC3D(self.hojas[1].nombre)
        self.hojas[5].crear_codigo3d(tabla_simbolos)

        texto = "void " + self.hojas[1].nombre + "(){\n"
        texto += self.hojas[5].expresion
        texto += "}\n"
        self.expresion = texto
        tabla_simbolos.eliminar_entorno()


