from parser.nodos.Nodo import Nodo
from parser.entorno.Tipos import SymbolType, DataType

class InstruccionAsignacion(Nodo):
    def __init__(self, token, id_nodo):
        super().__init__(token, id_nodo)

    def ejecutar(self, entorno):
        # a = expresion
        self.hojas[2].ejecutar(entorno)
        entorno.agregarVariable(self.hojas[0].nombre, self.hojas[2].valor, self.hojas[2].tipo, SymbolType.variable)

    def crear_tabla_simbolos(self, tabla_simbolos):
        tabla_simbolos.agregar_simbolo(self.hojas[0].nombre, DataType.generico, SymbolType.variable, 1)

    def crear_codigo3d(self, tabla_simbolos):
        # a = expresion
        self.hojas[2].crear_codigo3d(tabla_simbolos)
        posicionStack = tabla_simbolos.buscar_posicion(self.hojas[0].nombre)['posicionStack']
        texto = self.hojas[2].expresion + "\n"
        self.referencia = self.obtener_temporal()
        texto += str(self.referencia) + " = P + " + str(posicionStack) + "\n"
        texto += "stack[(int)" + str(self.referencia) + "]" + " = " + str(self.hojas[2].referencia) + "\n"
        self.expresion = texto


