from parser.nodos.Nodo import Nodo
from parser.entorno.Tipos import *
from parser.entorno.Entorno import Entorno

class InstruccionModulo(Nodo):
    def __init__(self, token, id_nodo):
        super().__init__(token, id_nodo)

    def ejecutar(self, entorno):
        # MOD IDENTIFICADOR  { instrucciones }
        ne = Entorno("entorno modulo")
        # no se asigna el anterior
        # ne.asignarAnterior(entorno)
        self.hojas[3].ejecutar(ne)
        entorno.agregarVariable(self.hojas[1].nombre, # Identificador
                                None, # No tiene ningun valor el módulo
                                DataType.generico, # Generico, si son funciones tipo "int", se colocarían acá.
                                SymbolType.modulo, # Modulo
                                ne.tabla_simbolos
                                )

    def crear_tabla_simbolos(self, tabla_simbolos):
        pass

    def crear_codigo3d(self, tabla_simbolos):
        pass


