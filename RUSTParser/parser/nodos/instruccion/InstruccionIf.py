from parser.entorno.Tipos import DataType
from parser.nodos.Nodo import Nodo
from parser.entorno.Entorno import Entorno


class InstruccionIf(Nodo):
    def __init__(self, token, id_nodo):
        super().__init__(token, id_nodo)

    def ejecutar(self, entorno):
        # if expresion { instrucciones }
        # if expresion { instrucciones } else/elseif
        self.hojas[1].ejecutar(entorno)
        if self.hojas[1].tipo == DataType.boolean:
            if self.hojas[1].valor:
                # instrucciones
                ne = Entorno("entorno if")
                ne.asignarAnterior(entorno)
                self.hojas[3].ejecutar(ne)
                self.copiar_valorhoja(3)
                print("Entorno de if", ne.tabla_simbolos)
                return
            else:
                if len(self.hojas) == 6:
                    self.hojas[5].ejecutar(entorno)
                    self.copiar_valorhoja(5)
                return
        print('El tipo de dato debe ser booleano ')

    def crear_tabla_simbolos(self, tabla_simbolos):
        pass

    def crear_codigo3d(self, tabla_simbolos):
        pass
