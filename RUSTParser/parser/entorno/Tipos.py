import enum


class SymbolType(enum.Enum):
    variable = 0
    funcion = 1
    modulo = 2
    struct = 3
    retorno = 4


class DataType(enum.Enum):
    error = 0
    int64 = 1
    f64 = 2
    boolean = 3
    vector = 4
    generico = 5
    retorno = 6


global validator

validador = {
    '+': {
        DataType.int64: {
            DataType.int64: DataType.int64,
            DataType.f64: DataType.f64
        }
    },
    '-': {
        DataType.int64: {
            DataType.int64: DataType.int64,
            DataType.f64: DataType.f64
        }
    },
    '<': {
        DataType.int64: {
            DataType.int64: DataType.boolean,
        },
        DataType.f64: {
            DataType.f64: DataType.boolean
        },
    },
    '>': {
        DataType.int64: {
            DataType.int64: DataType.boolean,
        },
        DataType.f64: {
            DataType.f64: DataType.boolean
        },
    },
    '==': {
        DataType.int64: {
            DataType.int64: DataType.boolean,
        },
        DataType.f64: {
            DataType.f64: DataType.boolean
        },
    }
}

