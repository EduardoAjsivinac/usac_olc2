from glob import glob
from parser.nodos import *

global noNode
noNode = 0

reserved = {
    'if': 'IF',
    'else': 'ELSE',
    'vec': 'VEC',
    'mod': 'MOD',
    'fn': 'FN'
}

tokens = (
    'IF',
    'ELSE',
    'VEC',
    'MOD',
    'FN',
    'ADMI',
    'MAS',
    'MENOS',
    'POR',
    'DIVIDIDO',
    'DECIMAL',
    'ENTERO',
    'PTCOMA',
    'IGUAL',
    'IDENTIFICADOR',
    'CADENA',
    'LLAVEA',
    'LLAVEC',
    'CORCHA',
    'CORCHC',
    'MAYORQUE',
    'MENORQUE',
    'COMA',
    'PARA',
    'PARC',
    'DOSPTS'
)

t_IF = r'if'
t_MOD = r'mod'
t_FN = r'fn'
t_ADMI = r'!'
t_COMA = r','
t_MAS = r'\+'
t_MENOS = r'-'
t_POR = r'\*'
t_DIVIDIDO = r'/'
t_PTCOMA = r';'
t_IGUAL = r'='
t_LLAVEA = r'{'
t_LLAVEC = r'}'
t_PARA = r'\('
t_PARC = r'\)'
t_MAYORQUE = r'>'
t_MENORQUE = r'<'
t_CORCHA = r'\['
t_CORCHC = r'\]'
t_DOSPTS = r':'

def t_CADENA(t):
    r'\".*?\"'
    # Automata utilizado (ver grafico graphviz online)
    # digraph CADENA
    # {
    #     S1->S2[label = "''"];
    #     S2->S1[label = "''"];
    #     S2->S3[label = "{"];
    #     S3->S2[label = "{, }"];
    #     S2->S2[label = "Cualquier caracter"];
    #     S3->ERROR[label = "Cualquier caracter"];
    #     ERROR->ERROR[label = "Cualquier caracter"];
    # }
    t.value = t.value[1:-1]  # Quitamos las comillas
    nodoCadena = TerminalCadena(t, getNoNodo())
    estado = "S2"
    texto = ""
    for x in t.value:
        if x == "{":
            if estado == "S2":
                estado = "S3"
                continue
            elif estado == "S3":
                estado = "S2"
        elif x == "}":
            if estado == "S3":
                estado = "S2"
                nodoCadena.agregarTexto(texto, getNoNodo())
                nodoCadena.agregarFormato(getNoNodo())
                texto = ""
                continue
            else:
                estado = "ERROR"
        else:
            texto += x
    if texto != "":
        nodoCadena.agregarTexto(texto, getNoNodo())
    if estado == "S3":
        estado = "ERROR"
    if estado != "S2":
        print("Error con el formato de cadena", t.value)
        t.value = ""
        return t
    t.value = nodoCadena
    return t


def t_DECIMAL(t):
    r'\d+\.\d+'
    try:
        t.value = float(t.value)
    except:
        print("Error al parsear el flotante %d", t.value)
        t.value = 0
    return t


def t_ENTERO(t):
    r'\d+'
    try:
        t.value = int(t.value)
    except:
        print("Error al parsear el entero %d", t.value)
        t.value = 0
    return t


def t_IDENTIFICADOR(t):
    r'[a-zA-Z_][a-zA-Z_0-9]*'
    t.type = reserved.get(t.value, 'IDENTIFICADOR')  # Check for reserved words
    return t


t_ignore = " \t\n"


def t_error(t):
    print("Caracter invalido '%s'" % t.value[0])
    t.lexer.skip(1)


import ply.lex as lex

lexer = lex.lex()

precedence = (
    ('left', 'MAS', 'MENOS'),
    ('left', 'POR', 'DIVIDIDO')
)


def p_lista_instrucciones(t):
    '''instrucciones : instruccion instrucciones
                    | instruccion'''
    t[0] = InstruccionGenerico(t.slice[0], getNoNodo())
    if len(t) == 3:
        t[0].hojas = t[2].hojas
        t[0].hojas.insert(0, t[1])
    else:
        t[0].hojas.append(t[1])


def p_instruccion_if(t):
    '''instruccion : instrif'''
    t[0] = t[1]

def p_instruccion_mod(t):
    '''instruccion : instrmod'''
    t[0] = t[1]

def p_instruccion_fun(t):
    '''instruccion : instrfun'''
    t[0] = t[1]

def p_instruccion_asignacion(t):
    '''instruccion : asignacion PTCOMA'''
    t[0] = t[1]


def p_lista_instruccionesexp(t):
    '''instruccionesexp :  instruccion instruccionesexpfin
                    | instruccionesexpfin '''
    t[0] = InstruccionGenerico(t.slice[0], getNoNodo())
    if len(t) == 3:
        t[0].hojas.append(t[1])
        if t[2] is not None:
            t[0].hojas.append(t[2])
    else:
        if t[1] is not None:
            t[0].hojas.append(t[1])


def p_lista_instruccionesexpfin(t):
    '''instruccionesexpfin :  instruccionesexp
                    | expresion
                    | '''
    if len(t) == 2:
        t[0] = t[1]
    else:
        t[0] = None




def p_funcion_if(t):
    '''instrif : IF expresion LLAVEA instruccionesexp LLAVEC instrelse'''
    t[0] = InstruccionIf(t.slice[0], getNoNodo())
    t[0].hojas.append(TerminalGenerico(t.slice[1], getNoNodo()))
    t[0].hojas.append(t[2])
    t[0].hojas.append(TerminalGenerico(t.slice[3], getNoNodo()))
    t[0].hojas.append(t[4])
    t[0].hojas.append(TerminalGenerico(t.slice[5], getNoNodo()))
    if t[6] is not None:
        t[0].hojas.append(t[6])

def p_funcion_else(t):
    '''instrelse : ELSE LLAVEA instruccionesexp LLAVEC
                | instrelseif '''
    if len(t) == 5:
        t[0] = InstruccionElse(t.slice[0], getNoNodo())
        t[0].hojas.append(TerminalGenerico(t.slice[1], getNoNodo()))
        t[0].hojas.append(TerminalGenerico(t.slice[2], getNoNodo()))
        t[0].hojas.append(t[3])
        t[0].hojas.append(TerminalGenerico(t.slice[4], getNoNodo()))
    elif len(t) == 2:
        t[0] = t[1]
    else:
        t[0] = None

def p_funcion_elseif(t):
    '''instrelseif : ELSE IF expresion LLAVEA instruccionesexp LLAVEC instrelse
                | '''
    if len(t) == 8:
        t[0] = InstruccionElseIf(t.slice[0], getNoNodo())
        t[0].hojas.append(TerminalGenerico(t.slice[1], getNoNodo()))
        t[0].hojas.append(TerminalGenerico(t.slice[2], getNoNodo()))
        t[0].hojas.append(t[3])
        t[0].hojas.append(TerminalGenerico(t.slice[4], getNoNodo()))
        t[0].hojas.append(t[5])
        t[0].hojas.append(TerminalGenerico(t.slice[6], getNoNodo()))
        if t[7] is not None:
            t[0].hojas.append(t[7])
    else:
        t[0] = None

def p_funcion_asignacion(t):
    '''asignacion : IDENTIFICADOR IGUAL expresion'''
    t[0] = InstruccionAsignacion(t.slice[0], getNoNodo())
    t[0].hojas.append(TerminalIdentificador(t.slice[1], getNoNodo()))
    t[0].hojas.append(TerminalGenerico(t.slice[2], getNoNodo()))
    t[0].hojas.append(t[3])


def p_expresion_suma(t):
    '''expresion : expresion MAS expresion'''
    t[0] = NodoSuma(t.slice[0], getNoNodo())
    t[0].hojas.append(t[1])
    t[0].hojas.append(TerminalGenerico(t.slice[2], getNoNodo()))
    t[0].hojas.append(t[3])


def p_expresion_resta(t):
    '''expresion : expresion MENOS expresion'''
    t[0] = NodoResta(t.slice[0], getNoNodo())
    t[0].hojas.append(t[1])
    t[0].hojas.append(TerminalGenerico(t.slice[2], getNoNodo()))
    t[0].hojas.append(t[3])


def p_expresion_multiplicacion(t):
    '''expresion : expresion POR expresion'''
    t[0] = NodoMultiplicacion(t.slice[0], getNoNodo())
    t[0].hojas.append(t[1])
    t[0].hojas.append(TerminalGenerico(t.slice[2], getNoNodo()))
    t[0].hojas.append(t[3])


def p_expresion_division(t):
    '''expresion : expresion DIVIDIDO expresion'''
    t[0] = NodoDivision(t.slice[0], getNoNodo())
    t[0].hojas.append(t[1])
    t[0].hojas.append(TerminalGenerico(t.slice[2], getNoNodo()))
    t[0].hojas.append(t[3])


def p_expresion_mayor(t):
    '''expresion : expresion MAYORQUE expresion'''
    t[0] = NodoMayor(t.slice[0], getNoNodo())
    t[0].hojas.append(t[1])
    t[0].hojas.append(TerminalGenerico(t.slice[2], getNoNodo()))
    t[0].hojas.append(t[3])


def p_expresion_cadena(t):
    '''expresion : CADENA'''
    t[0] = NodoExpresion(t.slice[0],getNoNodo())
    t[0].hojas.append(t.slice[1].value)


def p_expresion_entero(t):
    '''expresion : ENTERO'''
    t[0] = NodoExpresion(t.slice[0], getNoNodo())
    t[0].hojas.append(TerminalEntero(t.slice[1], getNoNodo()))


def p_expresion_decimal(t):
    '''expresion : DECIMAL'''
    t[0] = NodoExpresion(t.slice[0], getNoNodo())
    t[0].hojas.append(TerminalDecimal(t.slice[1], getNoNodo()))



def p_expresion_identificador(t):
    '''expresion : IDENTIFICADOR accesovector
                | IDENTIFICADOR accesomodulo'''
    t[0] = NodoExpresion(t.slice[0], getNoNodo())
    print(t.slice[2])
    if t[2] is not None:
        if str(t.slice[2]) == "accesovector":
            nodoAcceso = NodoAccesoVector(t.slice[1], getNoNodo())
            nodoAcceso.hojas.append(TerminalIdentificador(t.slice[1], getNoNodo()))
            nodoAcceso.hojas.append(t[2])
            t[0].hojas.append(nodoAcceso)
        elif t.slice[2] == '':
            pass
    else:
        t[0].hojas.append(TerminalIdentificador(t.slice[1], getNoNodo()))


def p_expresion_if(t):
    '''expresion : instrif'''
    t[0] = NodoExpresion(t.slice[0], getNoNodo())
    t[0].hojas.append(t[1])


def p_expresion_vec_exp(t):
    '''expresion : instrvec'''
    t[0] = NodoExpresion(t.slice[0], getNoNodo())
    t[0].hojas.append(t[1])


def p_inst_modulo(t):
    '''instrmod : MOD IDENTIFICADOR LLAVEA instrucciones LLAVEC'''
    t[0] = InstruccionModulo(t.slice[0], getNoNodo())
    t[0].hojas.append(TerminalGenerico(t.slice[1], getNoNodo()))
    t[0].hojas.append(TerminalIdentificador(t.slice[2], getNoNodo()))
    t[0].hojas.append(TerminalGenerico(t.slice[3], getNoNodo()))
    t[0].hojas.append(t[4])
    t[0].hojas.append(TerminalGenerico(t.slice[5], getNoNodo()))


def p_inst_funcion(t):
    '''instrfun : FN IDENTIFICADOR PARA PARC LLAVEA instrucciones  LLAVEC'''
    t[0] = InstruccionFuncion(t.slice[0], getNoNodo())
    t[0].hojas.append(TerminalGenerico(t.slice[1], getNoNodo()))
    t[0].hojas.append(TerminalIdentificador(t.slice[2], getNoNodo()))
    t[0].hojas.append(TerminalGenerico(t.slice[3], getNoNodo()))
    t[0].hojas.append(TerminalGenerico(t.slice[4], getNoNodo()))
    t[0].hojas.append(TerminalGenerico(t.slice[5], getNoNodo()))
    t[0].hojas.append(t[6])
    t[0].hojas.append(TerminalGenerico(t.slice[7], getNoNodo()))


def p_instr_vector(t):
    '''instrvec : VEC ADMI CORCHA listexp CORCHC'''
    t[0] = NodoVector(t.slice[1], getNoNodo())
    t[0].hojas.append(TerminalGenerico(t.slice[1], getNoNodo()))
    t[0].hojas.append(TerminalGenerico(t.slice[3], getNoNodo()))
    t[0].hojas.append(t[4])
    t[0].hojas.append(TerminalGenerico(t.slice[5], getNoNodo()))

def p_acceso_modulo(t):
    '''accesomodulo :  accesomodulo DOSPTS DOSPTS IDENTIFICADOR
                | DOSPTS DOSPTS IDENTIFICADOR
                |'''
    if len(t) == 5:
        # listaexp [ 0 ]
        # listaexp = 1 2 3
        # listaexp = 1 2 3 0
        t[0] = t[1]
        t[0].hojas.append(t[3])
    elif len(t) == 4:
        # [ exp ]
        t[0] = NodoListaExpresion(t.slice[0], getNoNodo())
        t[0].hojas.append(t[2])
    else:
        t[0] = None

def p_acceso_vector(t):
    '''accesovector :  accesovector CORCHA expresion CORCHC
                | CORCHA expresion CORCHC
                |'''
    if len(t) == 5:
        # listaexp [ 0 ]
        # listaexp = 1 2 3
        # listaexp = 1 2 3 0
        t[0] = t[1]
        t[0].hojas.append(t[3])
    elif len(t) == 4:
        # [ exp ]
        t[0] = NodoListaExpresion(t.slice[0], getNoNodo())
        t[0].hojas.append(t[2])
    else:
        t[0] = None




def p_listexpresion(t):
    '''listexp : listexp COMA expresion
                | expresion'''
    t[0] = NodoListaExpresion(t.slice[0], getNoNodo())
    if len(t) == 4:
        t[0].hojas = t[1].hojas
        t[0].hojas.append(t[3])
    else:
        t[0].hojas.append(t[1])


def p_error(t):
    print(t.type)
    print("Error sintáctico %s" % t.value)


import ply.yacc as yacc

parser = yacc.yacc()


def getNoNodo():
    global noNode
    noNode = noNode + 1
    return noNode


def analizar_entrada(entrada_texto):
    return parser.parse(entrada_texto)
