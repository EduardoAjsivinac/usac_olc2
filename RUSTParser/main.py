# --------------------------------------
# Universidad de San Carlos de Guatemala
# Ingenieria en Ciencias y Sistemas
# Eduardo Isai Ajsivinac Xico
# 201503584
# RUST Parser
from parser.entorno.Entorno import Entorno
from parser.gramatica import analizar_entrada
from parser.entorno.TablaSimbolosC3D import TablaSimbolosC3D

def print_hi(text_input):
    raiz = analizar_entrada(text_input)
    print(raiz.obtener_dot())
    tabla_simbolos = TablaSimbolosC3D("Global")
    raiz.crear_tabla_simbolos(tabla_simbolos)
    print(tabla_simbolos.entornos)
    # raiz.crear_tabla_simbolos(tabla_simbolos)
    tabla_simbolos.no_entorno = 0
    raiz.crear_codigo3d(tabla_simbolos)

    print(raiz.expresion)
    #entorno = Entorno("Global")
    #raiz.ejecutar(entorno) # Generamos objetos
    #nodoMain = entorno.obtenerValor("main")
    #if nodoMain is not None:
    #    ne = Entorno("FuncionMain")
    #    ne.asignarAnterior(entorno)
        # En entorno están las funciones, modulos.

        # ne se van a guardar las nuevaws variables.
    #    nodoMain['valor'].ejecutar(ne)
    #    print("-----------------------------------------------------------------")
    #    print(ne.tabla_simbolos)
    #    print("-----------------------------------------------------------------")
    #else:
    #    print("No hay funcion main")
    #print("Entorno Global", entorno.tabla_simbolos)


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    archivo = open("./entradas/entrada.rs")
    linea = archivo.readline()
    texto_entrada = linea
    while linea != '':
        linea = archivo.readline()
        texto_entrada += linea
    print_hi(texto_entrada)

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
